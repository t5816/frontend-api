import axios from 'axios'


const journalApi = axios.create({
    baseURL: 'https://vue-journal-a523e-default-rtdb.firebaseio.com/'
})

export default journalApi;